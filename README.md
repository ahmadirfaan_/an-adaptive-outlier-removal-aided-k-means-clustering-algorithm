# An Adaptive Outlier Removal Aided K-Means Clustering Algorithm

An algorithm design and analysis final project, reviewing a scientific paper discussing about algorithm. 

I chose https://www.sciencedirect.com/science/article/pii/S1319157821001701 as my main reference

The paper discusses about a K-Means Clustering Algorithm with the help of an adaptive outlier removal.
